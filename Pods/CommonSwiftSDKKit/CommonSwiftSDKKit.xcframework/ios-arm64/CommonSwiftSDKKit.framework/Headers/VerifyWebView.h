//
//  VerifyWebView.h
//  CommonSwiftSDKKit
//
//  Created by 杨鹏 on 2023/5/29.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef void(^VerifySuccess)(id messageBody);
typedef void(^VerifyFail)(NSError *error);

@interface VerifyWebView : UIView

- (void)loadRequest:(NSString *)urlStr verifySuccess:(VerifySuccess)successBlock verifyFail:(VerifyFail)failBlock;

@end

NS_ASSUME_NONNULL_END
