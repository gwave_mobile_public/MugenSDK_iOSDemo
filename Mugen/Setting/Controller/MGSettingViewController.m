//
//  MGAccountManageViewController.m
//  Mugen
//
//  Created by 杨鹏 on 2023/5/22.
//

#import "MGSettingViewController.h"
#import "MGSettingTableViewCell.h"
#import <MugenContainer/MugenContainer.h>

@interface MGSettingViewController ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) MugenContainerCustomerVO *userInfo;

@end

@implementation MGSettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"Setting";
    self.view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.tableView];
    [self userInfoRequest];
}

- (void)tableViewReload
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
    });
}

- (void)goBackMain:(MGMainType)mainType
{
    dispatch_async(dispatch_get_main_queue(), ^{
        if (self.settingGobackBlock) {
            self.settingGobackBlock(mainType);
        }
        [self.navigationController popViewControllerAnimated:YES];
    });
}

- (void)userInfoRequest
{
    dispatch_async(dispatch_get_main_queue(), ^{
        MugenContainerLoginRepository *loginRepository = [[MugenContainerLoginRepository alloc] init];
        __weak typeof(self) weakSelf = self;
        [loginRepository getUserInfoWithCompletionHandler:^(MugenContainerUserInfo * _Nullable response, NSError * _Nullable error) {
            if (response.customerInfo.success.boolValue) {
                weakSelf.userInfo = response.customerInfo.obj;
                [weakSelf tableViewReload];
            } else {
                NSLog(@"获取用户登录信息失败，%@",response.customerInfo.msgKey);
            }
        }];
    });
}

- (void)logoutRequest
{
    dispatch_async(dispatch_get_main_queue(), ^{
        MugenContainerLoginRepository *loginRepository = [[MugenContainerLoginRepository alloc] init];
        __weak typeof(self) weakSelf = self;
        [loginRepository logoutWithCompletionHandler:^(MugenContainerWebResponse * _Nullable response, NSError * _Nullable error) {
            if (response.success.boolValue) {
                weakSelf.userInfo = nil;
                [weakSelf tableViewReload];
                [weakSelf goBackMain:MGMainTypeLogout];
            } else {
                NSLog(@"退出登录失败，%@",response.msgKey);
            }
        }];
    });
}


#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 1;
    } else if (section == 1){
        return 3;
    } else {
        return 1;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [tableView dequeueReusableCellWithIdentifier:@"MGSettingTableViewCell"];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    MGSettingTableViewCell *settingCell = (MGSettingTableViewCell *)cell;

    if (indexPath.section == 0) {
        [settingCell fillData:self.userInfo settingType:MGSettingTypeUserInfo];
    } else if (indexPath.section == 1)  {
        if (indexPath.row == 0) {
            [settingCell fillData:self.userInfo settingType:MGSettingTypeEmail];
        } else if (indexPath.row == 1) {
            [settingCell fillData:self.userInfo settingType:MGSettingTypeWeb3Auth];
        } else if (indexPath.row == 2) {
            [settingCell fillData:self.userInfo settingType:MGSettingTypeAccount];
        }
    } else {
        [settingCell fillData:self.userInfo settingType:MGSettingTypeLogOut];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        
    } else if (indexPath.section == 1)  {
        MGMainType mainType;
        if (indexPath.row == 0) {
            mainType = self.userInfo.isBindEmail.integerValue == 0 ? MGMainTypeBindEmail : MGMainTypeChangeBindEmail;
        } else if (indexPath.row == 1) {
            mainType = self.userInfo.isBindEmail.integerValue == 0 ? MGMainTypeBindWeb3Auth : MGMainTypeChangeBindWeb3Auth;
        } else {
            mainType = MGMainTypeUpdateAccount;
        }
        [self goBackMain:mainType];
    } else {
        [self logoutRequest];
    }
}

#pragma mark - getter

- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, UIScreen.mainScreen.bounds.size.width, UIScreen.mainScreen.bounds.size.height) style:UITableViewStyleGrouped];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        [_tableView registerClass:[MGSettingTableViewCell class] forCellReuseIdentifier:@"MGSettingTableViewCell"];
        _tableView.estimatedSectionHeaderHeight = 0;
        _tableView.estimatedSectionFooterHeight = 0;
    }
    return _tableView;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
