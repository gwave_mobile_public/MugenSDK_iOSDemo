//
//  MGAccountManageViewController.h
//  Mugen
//
//  Created by 杨鹏 on 2023/5/22.
//

#import <UIKit/UIKit.h>
#import "define.h"

NS_ASSUME_NONNULL_BEGIN

typedef void(^SettingGoBackBlock)(MGMainType mainType);

@interface MGSettingViewController : UIViewController

@property (nonatomic, copy) SettingGoBackBlock settingGobackBlock;

@end

NS_ASSUME_NONNULL_END
