//
//  MGSettingTableViewCell.m
//  Mugen
//
//  Created by 杨鹏 on 2023/5/22.
//

#import "MGSettingTableViewCell.h"
#import <Masonry/Masonry.h>

@interface MGSettingTableViewCell()

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *detailLabel;
@property (nonatomic, strong) UIImageView *arrowImage;

@end

@implementation MGSettingTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setUI];
    }
    return self;
}

- (void)setUI
{
    self.selectionStyle = UITableViewCellSelectionStyleNone;

    [self.contentView addSubview:self.titleLabel];
    [self.contentView addSubview:self.detailLabel];
    [self.contentView addSubview:self.arrowImage];
    
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView.mas_left).offset(20);
        make.centerY.equalTo(self.contentView.mas_centerY);
        make.height.equalTo(self.contentView.mas_height);
    }];
    
    [self.arrowImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView.mas_right).offset(-15);
        make.centerY.equalTo(self.contentView.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(15, 20));
    }];
    
    [self.detailLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.arrowImage.mas_left).offset(-10);
        make.centerY.equalTo(self.contentView.mas_centerY);
        make.height.equalTo(self.contentView.mas_height);
    }];
}

-(void)fillData:(MugenContainerCustomerVO *)userInfo settingType:(MGSettingType)settingType
{
    switch (settingType) {
        case MGSettingTypeUserInfo:
            self.titleLabel.text = @"UserInfo";
            self.detailLabel.hidden = NO;
            self.detailLabel.text = userInfo.id.length > 0 ? userInfo.id : @"";
            break;
        case MGSettingTypeEmail:{
            NSString *emailStr = userInfo.isBindEmail.integerValue == 0 ? @"Email" : [NSString stringWithFormat:@"Email：%@",userInfo.email];
            self.titleLabel.text = emailStr;
            self.detailLabel.hidden = NO;
            self.detailLabel.text = userInfo.isBindEmail.integerValue == 0 ? @"绑定" : @"换绑";
        }
            
            break;
        case MGSettingTypeWeb3Auth:
            self.titleLabel.text = @"Web3Auth";
            self.detailLabel.hidden = NO;
            self.detailLabel.text = userInfo.isBindWeb3.integerValue == 0 ? @"绑定" : @"换绑";
            break;
        case MGSettingTypeAccount:
            self.titleLabel.text = @"切换账户";
            self.detailLabel.hidden = NO;
            self.detailLabel.text = @"";
            break;
        case MGSettingTypeLogOut:
            self.titleLabel.text = @"退出登录";
            self.detailLabel.hidden = YES;
            self.detailLabel.text = @"";
            break;
        default:
            break;
    }
}

#pragma mark -getter
- (UILabel *)titleLabel
{
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.text = @"title";
        _titleLabel.textAlignment = NSTextAlignmentLeft;
        _titleLabel.textColor = [UIColor blackColor];
        _titleLabel.font = [UIFont systemFontOfSize:16];
    }
    return _titleLabel;
}

- (UILabel *)detailLabel
{
    if (!_detailLabel) {
        _detailLabel = [[UILabel alloc] init];
        _detailLabel.text = @"detailLabel";
        _detailLabel.textAlignment = NSTextAlignmentLeft;
        _detailLabel.textColor = [UIColor blackColor];
        _detailLabel.font = [UIFont systemFontOfSize:16];
    }
    return _detailLabel;
}

- (UIImageView *)arrowImage
{
    if (!_arrowImage) {
        _arrowImage = [[UIImageView alloc] init];
        _arrowImage.image = [UIImage imageNamed:@"arrow_image"];
    }
    return _arrowImage;
}

@end
