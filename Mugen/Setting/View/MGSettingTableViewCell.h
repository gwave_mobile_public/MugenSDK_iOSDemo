//
//  MGSettingTableViewCell.h
//  Mugen
//
//  Created by 杨鹏 on 2023/5/22.
//

#import <UIKit/UIKit.h>
#import "define.h"
#import <MugenContainer/MugenContainer.h>

NS_ASSUME_NONNULL_BEGIN

@interface MGSettingTableViewCell : UITableViewCell

-(void)fillData:(MugenContainerCustomerVO *)userInfo settingType:(MGSettingType)settingType;

@end

NS_ASSUME_NONNULL_END
