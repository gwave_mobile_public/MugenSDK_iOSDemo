//
//  MGPushViewController.m
//  Mugen
//
//  Created by 杨鹏 on 2023/6/28.
//

#import "MGPushViewController.h"
#import <Masonry/Masonry.h>
#import <MugenContainer/MugenContainer.h>
#import <UserNotifications/UserNotifications.h>
#import <SVProgressHUD/SVProgressHUD.h>

@interface MGPushViewController ()

@property (nonatomic, copy) UIButton *addLocalPushBtn;
@property (nonatomic, copy) UIButton *cancelLocalPushBtn;

@property (nonatomic, copy) UIButton *addLocalPushBtn1;
@property (nonatomic, copy) UIButton *cancelLocalPushBtn1;
@end


@implementation MGPushViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"Push";
    self.view.backgroundColor = [UIColor whiteColor];
    [self setupUI];
}

- (void)setupUI
{
    [self.view addSubview:self.addLocalPushBtn];
    [self.view addSubview:self.cancelLocalPushBtn];
    [self.view addSubview:self.addLocalPushBtn1];
    [self.view addSubview:self.cancelLocalPushBtn1];
    
    [self.addLocalPushBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view.mas_top).offset(120);
        make.left.equalTo(self.view.mas_left).offset(20);
        make.size.mas_equalTo(CGSizeMake((UIScreen.mainScreen.bounds.size.width - 60)/2, 40));
    }];
    
    [self.cancelLocalPushBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view.mas_top).offset(120);
        make.left.equalTo(self.addLocalPushBtn.mas_right).offset(20);
        make.size.mas_equalTo(CGSizeMake((UIScreen.mainScreen.bounds.size.width - 60)/2, 40));
    }];
    
    [self.addLocalPushBtn1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.addLocalPushBtn.mas_bottom).offset(20);
        make.left.equalTo(self.view.mas_left).offset(20);
        make.size.mas_equalTo(CGSizeMake((UIScreen.mainScreen.bounds.size.width - 60)/2, 40));
    }];
    
    [self.cancelLocalPushBtn1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.addLocalPushBtn.mas_bottom).offset(20);
        make.left.equalTo(self.addLocalPushBtn1.mas_right).offset(20);
        make.size.mas_equalTo(CGSizeMake((UIScreen.mainScreen.bounds.size.width - 60)/2, 40));
    }];
}

#pragma mark -action
- (void)addLocalPush:(UIButton *)btn
{
    MugenContainerNotificationUtils *utils = [[MugenContainerNotificationUtils alloc] init];
    [utils startAlarmNotificationContext:nil time:30 title:[NSString stringWithFormat:@"测试标题%li",(long)btn.tag] body:@"这是个描述" code:(int)btn.tag];
    [SVProgressHUD showSuccessWithStatus:@"Add Local Notification Success"];
//    UNMutableNotificationContent *content = [[UNMutableNotificationContent alloc] init];
//    content.title = @"友情提示";
//    content.body = @"这是一条本地推送";
//    content.sound = UNNotificationSound.defaultSound;
//
//    NSDate *date = [NSDate dateWithTimeIntervalSinceNow:60]; // 触发时间为当前时间之后 1 小时
//    NSCalendar *calendar = [NSCalendar currentCalendar];
//    NSDateComponents *components = [calendar components:NSCalendarUnitYear+NSCalendarUnitMonth+NSCalendarUnitDay+NSCalendarUnitHour+NSCalendarUnitMinute+NSCalendarUnitSecond fromDate:date];
//    UNCalendarNotificationTrigger *trigger = [UNCalendarNotificationTrigger triggerWithDateMatchingComponents:components repeats:NO];
//
////    NSString *identifier = [[NSUUID UUID] UUIDString];
//    NSString *identifier = @"1234567890";
//    UNNotificationRequest *request = [UNNotificationRequest requestWithIdentifier:identifier content:content trigger:trigger];
//    UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
//    [center addNotificationRequest:request withCompletionHandler:^(NSError * _Nullable error) {
//        if (error) {
//            NSLog(@"Failed to add notification request: %@", error.localizedDescription);
//        } else {
//            NSLog(@"Added a notification request with identifier: %@", identifier);
//        }
//    }];
    
}

- (void)cancelLocalPush:(UIButton *)btn
{
//    UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
//    [center removePendingNotificationRequestsWithIdentifiers:@[@"1000"]];
    MugenContainerNotificationUtils *utils = [[MugenContainerNotificationUtils alloc] init];
    [utils cancelAlarmNotificationContext:nil code:(int)btn.tag];
    [SVProgressHUD showSuccessWithStatus:@"Remove Local Notification Success"];

}


#pragma mark -getter
- (UIButton *)addLocalPushBtn
{
    if (!_addLocalPushBtn) {
        _addLocalPushBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_addLocalPushBtn setTitle:@"Add local Push 1" forState:UIControlStateNormal];
        [_addLocalPushBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _addLocalPushBtn.backgroundColor = [UIColor purpleColor];
        _addLocalPushBtn.titleLabel.font = [UIFont systemFontOfSize:13];
        _addLocalPushBtn.layer.cornerRadius = 4.0f;
        _addLocalPushBtn.tag = 1;
        [_addLocalPushBtn addTarget:self action:@selector(addLocalPush:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _addLocalPushBtn;
}

- (UIButton *)cancelLocalPushBtn
{
    if (!_cancelLocalPushBtn) {
        _cancelLocalPushBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_cancelLocalPushBtn setTitle:@"Cancel local Push 1" forState:UIControlStateNormal];
        [_cancelLocalPushBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _cancelLocalPushBtn.backgroundColor = [UIColor purpleColor];
        _cancelLocalPushBtn.titleLabel.font = [UIFont systemFontOfSize:13];
        _cancelLocalPushBtn.layer.cornerRadius = 4.0f;
        _cancelLocalPushBtn.tag = 1;
        [_cancelLocalPushBtn addTarget:self action:@selector(cancelLocalPush:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _cancelLocalPushBtn;
}

- (UIButton *)addLocalPushBtn1
{
    if (!_addLocalPushBtn1) {
        _addLocalPushBtn1 = [UIButton buttonWithType:UIButtonTypeCustom];
        [_addLocalPushBtn1 setTitle:@"Add local Push 2" forState:UIControlStateNormal];
        [_addLocalPushBtn1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _addLocalPushBtn1.backgroundColor = [UIColor purpleColor];
        _addLocalPushBtn1.titleLabel.font = [UIFont systemFontOfSize:13];
        _addLocalPushBtn1.layer.cornerRadius = 4.0f;
        _addLocalPushBtn1.tag = 2;
        [_addLocalPushBtn1 addTarget:self action:@selector(addLocalPush:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _addLocalPushBtn1;
}

- (UIButton *)cancelLocalPushBtn1
{
    if (!_cancelLocalPushBtn1) {
        _cancelLocalPushBtn1 = [UIButton buttonWithType:UIButtonTypeCustom];
        [_cancelLocalPushBtn1 setTitle:@"Cancel local Push 2" forState:UIControlStateNormal];
        [_cancelLocalPushBtn1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _cancelLocalPushBtn1.backgroundColor = [UIColor purpleColor];
        _cancelLocalPushBtn1.titleLabel.font = [UIFont systemFontOfSize:13];
        _cancelLocalPushBtn1.layer.cornerRadius = 4.0f;
        _cancelLocalPushBtn1.tag = 2;
        [_cancelLocalPushBtn1 addTarget:self action:@selector(cancelLocalPush:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _cancelLocalPushBtn1;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
