//
//  AppDelegate.m
//  Mugen
//
//  Created by 杨鹏 on 2023/5/9.
//

#import "AppDelegate.h"
#import <MugenContainer/MugenContainer.h>
#import "MGHomeViewController.h"
#import "MGInviteViewController.h"
#import "UIViewController+CDFUtils.h"
#import <UserNotifications/UserNotifications.h>

@interface AppDelegate ()<UNUserNotificationCenterDelegate>

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    MugenContainerFrameworkFirebase *firebase = [[MugenContainerFrameworkFirebase alloc] init];
    [firebase configure];
        
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    MGHomeViewController *homeVc = [[MGHomeViewController alloc] init];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:homeVc];
    self.window.rootViewController = nav;
    self.window.backgroundColor=[UIColor whiteColor];
    UINavigationBar *navigationbar = [UINavigationBar appearance];
    UINavigationBarAppearance *appearance = [[UINavigationBarAppearance alloc] init];
    appearance.backgroundColor = [UIColor whiteColor];
    appearance.titleTextAttributes = @{NSFontAttributeName: [UIFont boldSystemFontOfSize:16],NSForegroundColorAttributeName:[UIColor blackColor]};
    navigationbar.standardAppearance = appearance;
    navigationbar.scrollEdgeAppearance = appearance;
    [self.window makeKeyAndVisible];
    
    UNAuthorizationOptions options = UNAuthorizationOptionSound|UNAuthorizationOptionBadge|UNAuthorizationOptionAlert;
    UNUserNotificationCenter.currentNotificationCenter.delegate = self;
    [UNUserNotificationCenter.currentNotificationCenter requestAuthorizationWithOptions:options completionHandler:^(BOOL granted, NSError * _Nullable error) {
        if (error || granted == NO) {
            NSLog(@"[APNS] granted:%d, error:%@", granted, error);
        } else {
            dispatch_async(dispatch_get_main_queue(), ^{
                [UIApplication.sharedApplication registerForRemoteNotifications];
            });
        }
    }];
    
    NSSet *patterns = [[NSSet alloc] initWithObjects:UIPasteboardDetectionPatternProbableWebURL, nil];
    [[UIPasteboard generalPasteboard] detectPatternsForPatterns:patterns completionHandler:^(NSSet<UIPasteboardDetectionPattern> * _Nullable result, NSError * _Nullable error) {
        if (result && result.count > 0) {
            UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
            NSString *urlString = [pasteboard.string copy];
            [firebase handleUniversalLinkUrl:[NSURL URLWithString:urlString] completion:^(NSDictionary<NSString *,NSString *> * _Nullable params) {
                NSLog(@"params = %@",params);
                NSString *type = [params objectForKey:@"type"];
                if ([type isEqualToString:@"invitationRebate"]) {
                    MGInviteViewController *ivc = [[MGInviteViewController alloc] init];
                    ivc.paramsDict = params;
                    [[UIViewController currentNavigationController] pushViewController:ivc animated:YES];
                }
            }];
        }
    }];

    return YES;
}

- (BOOL)application:(UIApplication *)application continueUserActivity:(NSUserActivity *)userActivity restorationHandler:(void (^)(NSArray<id<UIUserActivityRestoring>> * _Nullable))restorationHandler
{
    MugenContainerFrameworkFirebase *firebase = [[MugenContainerFrameworkFirebase alloc] init];
    BOOL handled =  [firebase handleUniversalLinkUrl:userActivity.webpageURL completion:^(NSDictionary<NSString *,NSString *> * _Nullable params) {
        NSLog(@"params = %@",params);
        NSString *type = [params objectForKey:@"type"];
        if ([type isEqualToString:@"invitationRebate"]) {
            MGInviteViewController *ivc = [[MGInviteViewController alloc] init];
            ivc.paramsDict = params;
            [[UIViewController currentNavigationController] pushViewController:ivc animated:YES];
        }
    }];
    return handled;
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    MugenContainerImClient *imclient = [[MugenContainerImClient alloc] init];
    [imclient imPushToken:deviceToken];
}

#pragma mark -UNUserNotificationCenterDelegate

- (void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler {
    completionHandler(UNNotificationPresentationOptionBadge |
                      UNNotificationPresentationOptionSound |
                      UNNotificationPresentationOptionList | UNNotificationPresentationOptionBanner);
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void(^)(void))completionHandler {
    
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center openSettingsForNotification:(nullable UNNotification *)notification {
    
}

@end
