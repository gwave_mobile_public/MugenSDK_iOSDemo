//
//  MGIapViewController.m
//  Mugen
//
//  Created by 杨鹏 on 2023/5/30.
//

#import "MGIapViewController.h"
#import <MugenContainer/MugenContainer.h>
#import "MGIapListTableViewCell.h"
#import <SVProgressHUD/SVProgressHUD.h>

@interface MGIapViewController ()<UITableViewDelegate, UITableViewDataSource, MugenContainerProductDetailsResponseInterface, MugenContainerOnPurchaseInterface>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSArray *productsArray;
@end


@implementation MGIapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"IAP";
    self.view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.tableView];
    //获取苹果后台配置的内购商品
    [self getIapProducts];
}

- (void)getIapProducts
{
    [SVProgressHUD showWithStatus:@"Request Products"];
    NSArray *products =@[@"com.mugen.odyssey_iap_appstore_4.99_gem"];
    [[MugenContainerInAppPay.companion INSTANCE] queryLostPurchaseListener:self completionHandler:^(NSError * _Nullable error) {
        
    }];
    [[MugenContainerInAppPay.companion INSTANCE] requestIapProductsIdentifies:products listener:self completionHandler:^(NSError * _Nullable error) {
            
    }];
}

#pragma mark - MugenContainerProductDetailsResponseInterface

- (void)onSuccessProductDetails:(NSArray<MugenContainerProductDetails *> *)productDetails
{
    [SVProgressHUD dismiss];
    dispatch_async(dispatch_get_main_queue(), ^{
        if(productDetails.count > 0) {
            self.productsArray = productDetails;
            [self.tableView reloadData];
        }
    });
}

- (void)onFailureErrorMsg:(NSString *)errorMsg
{
    [SVProgressHUD showErrorWithStatus:errorMsg];
    NSLog(@"responseProductIds = %@", errorMsg);
}

#pragma mark - MugenContainerOnPurchaseInterface

- (void)onPaySuccessOrderId:(NSString *)orderId
{
    NSString *remindStr = [NSString stringWithFormat:@"Verify Success, Payment Finish, OrderId is %@",orderId];
    [SVProgressHUD showSuccessWithStatus:remindStr];
    NSLog(@"Verify Order Success = %@",orderId);
}

- (void)onPayFailureErrorMsg:(NSString *)errorMsg
{
    [SVProgressHUD showErrorWithStatus:errorMsg];
    NSLog(@"Payment Error = %@", errorMsg);
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.productsArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [tableView dequeueReusableCellWithIdentifier:@"MGIapListTableViewCell"];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    MGIapListTableViewCell *listCell = (MGIapListTableViewCell *)cell;
    MugenContainerProductDetails *product= [self.productsArray objectAtIndex:indexPath.row];
    [listCell setProduct:product];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 90;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [SVProgressHUD showWithStatus:@"Waiting For Payment"];
    MugenContainerProductDetails *product= [self.productsArray objectAtIndex:indexPath.row];
    //创建订单
    [[MugenContainerInAppPay.companion INSTANCE] startPurchaseProductIdentifier:product.productId activity:nil listener:self completionHandler:^(NSError * _Nullable error) {
            
    }];
}

#pragma mark - getter

- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, UIScreen.mainScreen.bounds.size.width, UIScreen.mainScreen.bounds.size.height) style:UITableViewStyleGrouped];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        [_tableView registerClass:[MGIapListTableViewCell class] forCellReuseIdentifier:@"MGIapListTableViewCell"];
        _tableView.estimatedSectionHeaderHeight = 0;
        _tableView.estimatedSectionFooterHeight = 0;
    }
    return _tableView;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
