//
//  MGIapListTableViewCell.h
//  Mugen
//
//  Created by 杨鹏 on 2023/5/30.
//

#import <UIKit/UIKit.h>
#import <MugenContainer/MugenContainer.h>

NS_ASSUME_NONNULL_BEGIN

@interface MGIapListTableViewCell : UITableViewCell

@property (nonatomic, strong) MugenContainerProductDetails *product;

@end

NS_ASSUME_NONNULL_END
