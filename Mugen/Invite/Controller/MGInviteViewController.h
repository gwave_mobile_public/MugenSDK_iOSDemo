//
//  MGInviteViewController.h
//  Mugen
//
//  Created by 杨鹏 on 2023/6/15.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MGInviteViewController : UIViewController

@property (nonatomic, strong) NSDictionary *paramsDict;

@end

NS_ASSUME_NONNULL_END
