//
//  MGInviteViewController.m
//  Mugen
//
//  Created by 杨鹏 on 2023/6/15.
//

#import "MGInviteViewController.h"
#import <Masonry/Masonry.h>
#import <MugenContainer/MugenContainer.h>
#import <SVProgressHUD/SVProgressHUD.h>
#import "MGUserManager.h"

@interface MGInviteViewController ()

@property (nonatomic, copy) UIButton *inviteCodeBtn;
@property (nonatomic, copy) UIButton *shareBtn;
@property (nonatomic, copy) UILabel *inviteCodeLabel;
@property (nonatomic, copy) NSString *inviteCode;
@end

@implementation MGInviteViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"Invite";
    self.view.backgroundColor = [UIColor whiteColor];
    [self setupUI];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(inviteBind) name:@"loginSuccess" object:nil];
}

- (void)setupUI
{
    [self.view addSubview:self.inviteCodeBtn];
    [self.view addSubview:self.inviteCodeLabel];
    [self.view addSubview:self.shareBtn];
    
    [self.inviteCodeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view.mas_top).offset(120);
        make.left.equalTo(self.view.mas_left).offset(20);
        make.size.mas_equalTo(CGSizeMake((UIScreen.mainScreen.bounds.size.width - 60)/2, 40));
    }];
    
    [self.inviteCodeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.inviteCodeBtn.mas_bottom).offset(20);
        make.left.equalTo(self.view.mas_left).offset(20);
        make.size.mas_equalTo(CGSizeMake(UIScreen.mainScreen.bounds.size.width - 40, 40));
    }];
    
    [self.shareBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view.mas_top).offset(120);
        make.left.equalTo(self.inviteCodeBtn.mas_right).offset(20);
        make.size.mas_equalTo(CGSizeMake((UIScreen.mainScreen.bounds.size.width - 60)/2, 40));
    }];
}

- (void)setParamsDict:(NSDictionary *)paramsDict
{
    _paramsDict = paramsDict;
    //获取到邀请码，和邀请人进行绑定流程
    self.inviteCode = [paramsDict objectForKey:@"code"];
    //game client与game Sever确认是否可以绑定关系
    
    //game确定可以绑定关系后，调用此方法通知SDK绑定
    [self inviteBind];
}

- (void)inviteBind
{
    if (self.inviteCode.length > 0 && [MGUserManager sharedInstance].userInfo) {
        [self showInviteCode:[NSString stringWithFormat:@"inviteCode = %@",self.inviteCode]];
        dispatch_async(dispatch_get_main_queue(), ^{
            MugenContainerInviteRepository *inviteRepository = [[MugenContainerInviteRepository alloc] init];
            [inviteRepository inviteBindInviteCode:self.inviteCode completionHandler:^(MugenContainerWebResultVoid * _Nullable inviteResponse, NSError * _Nullable error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (inviteResponse.success.boolValue) {
                        [SVProgressHUD showSuccessWithStatus:@"Bind Success"];
                    } else {
                        [SVProgressHUD showErrorWithStatus:inviteResponse.msgKey];
                    }
                });
            }];
        });
    } else {
        NSLog(@"not login");
    }
}

- (void)showInviteCode:(NSString *)invite
{
    dispatch_async(dispatch_get_main_queue(), ^{
        if (invite.length > 0) {
            self.inviteCodeLabel.hidden = NO;
            self.inviteCodeLabel.text = invite;
        } else {
            self.inviteCodeLabel.hidden = YES;
        }
    });
}

#pragma mark -action

- (void)createInviteCode
{
    //获取邀请码
    MugenContainerInviteRepository *inviteRepository = [[MugenContainerInviteRepository alloc] init];
    __weak typeof(self) weakSelf = self;
    [inviteRepository customerInviteWithCompletionHandler:^(MugenContainerWebResultCustomerInviteVO * _Nullable inviteResponse, NSError * _Nullable error) {
        if (inviteResponse.success.boolValue) {
            NSLog(@"inviteCode = %@, inviteId = %@",inviteResponse.obj.inviteCode, inviteResponse.obj.inviterId);
            NSString *invite = [NSString stringWithFormat:@"inviteCode = %@,inviteId = %@",inviteResponse.obj.inviteCode, inviteResponse.obj.inviterId];
            weakSelf.inviteCode = inviteResponse.obj.inviteCode;
            [weakSelf showInviteCode:invite];
        }
    }];
}

- (void)shareClick
{
    if (self.inviteCode.length > 0) {
        //生成邀请链接，调用分享
        MugenContainerInviteShare *inviteShare = [[MugenContainerInviteShare alloc] initWithShowPage:self];
        NSString *shareUrl = [NSString stringWithFormat:@"https://m.dipbit.xyz/test/test.html?code=%@",self.inviteCode];
        NSLog(@"shareUrl = %@",shareUrl);
        [inviteShare shareUrlContent:shareUrl title:@"快来下载吧！"];
    }
}

#pragma mark -getter
- (UIButton *)inviteCodeBtn
{
    if (!_inviteCodeBtn) {
        _inviteCodeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_inviteCodeBtn setTitle:@"InviteCode" forState:UIControlStateNormal];
        [_inviteCodeBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _inviteCodeBtn.backgroundColor = [UIColor purpleColor];
        _inviteCodeBtn.titleLabel.font = [UIFont systemFontOfSize:13];
        _inviteCodeBtn.layer.cornerRadius = 4.0f;
        [_inviteCodeBtn addTarget:self action:@selector(createInviteCode) forControlEvents:UIControlEventTouchUpInside];
    }
    return _inviteCodeBtn;
}

- (UILabel *)inviteCodeLabel
{
    if (!_inviteCodeLabel) {
        _inviteCodeLabel = [[UILabel alloc] init];
        _inviteCodeLabel.font = [UIFont systemFontOfSize:12];
        _inviteCodeLabel.textColor = [UIColor blackColor];
    }
    return _inviteCodeLabel;
}

- (UIButton *)shareBtn
{
    if (!_shareBtn) {
        _shareBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_shareBtn setTitle:@"Share" forState:UIControlStateNormal];
        [_shareBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _shareBtn.backgroundColor = [UIColor purpleColor];
        _shareBtn.titleLabel.font = [UIFont systemFontOfSize:13];
        _shareBtn.layer.cornerRadius = 4.0f;
        [_shareBtn addTarget:self action:@selector(shareClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _shareBtn;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
