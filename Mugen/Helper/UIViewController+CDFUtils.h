//
//  UIViewController+CDFUtils.h
//  CDFDemo
//
//  Created by lizenan on 2017/4/19.
//  Copyright © 2017年 lizenan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (CDFUtils)

/**
 * AppDelegate.window
 */
+ (UIViewController *)currentViewController;
/**
 * AppDelegate.window
 */
+ (UINavigationController *)currentNavigationController;

/**
 [UIApplication sharedApplication].keyWindow
 */
+(UIViewController *)currentKeyWindowViewController;

@end
