//
//  MGUserManager.m
//  Mugen
//
//  Created by 杨鹏 on 2023/7/6.
//

#import "MGUserManager.h"

@implementation MGUserManager

+ (instancetype)sharedInstance {
    static MGUserManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

@end
