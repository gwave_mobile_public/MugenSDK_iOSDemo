//
//  MGUserManager.h
//  Mugen
//
//  Created by 杨鹏 on 2023/7/6.
//

#import <Foundation/Foundation.h>
#import <MugenContainer/MugenContainer.h>

NS_ASSUME_NONNULL_BEGIN

@interface MGUserManager : NSObject

+ (instancetype)sharedInstance;

@property (nonatomic, strong) MugenContainerUserInfo *userInfo;

@end

NS_ASSUME_NONNULL_END
