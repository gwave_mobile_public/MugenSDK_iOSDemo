//
//  define.h
//  Mugen
//
//  Created by 杨鹏 on 2023/5/22.
//

#ifndef define_h
#define define_h

//setting 页面Cell枚举
typedef NS_ENUM(NSInteger, MGSettingType){
    MGSettingTypeUserInfo = 0,
    MGSettingTypeEmail,
    MGSettingTypeWeb3Auth,
    MGSettingTypeAccount,
    MGSettingTypeLogOut
};

//Home 页面Cell枚举
typedef NS_ENUM(NSInteger,MGHomeCellType){
    MGHomeCellTypeLogin = 0,
    MGHomeCellTypeIAP,
    MGHomeCellTypeWebView,
    MGHomeCellTypeInvite,
    MGHomeCellTypePush,
};

// Main首页调用接口枚举
typedef NS_ENUM(NSInteger, MGMainType){
    MGMainTypeDefault = 0, //默认
    MGMainTypeBindEmail, //绑定邮箱
    MGMainTypeChangeBindEmail,//换绑邮箱
    MGMainTypeBindWeb3Auth, //绑定web3Auth
    MGMainTypeChangeBindWeb3Auth, //换绑web3Auth
    MGMainTypeUpdateAccount,
    MGMainTypeLogout
};

typedef NS_ENUM(NSInteger, MGMainTextFieldType){
    MGMainTextFieldTypeDefault = 0, //默认
    MGMainTextFieldTypeOld, //旧邮箱验证
    MGMainTextFieldTypeNew,//新邮箱验证
};




#endif /* define_h */
