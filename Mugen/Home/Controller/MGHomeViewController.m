//
//  MGHomeViewController.m
//  Mugen
//
//  Created by 杨鹏 on 2023/5/30.
//

#import "MGHomeViewController.h"
#import "MGMainViewController.h"
#import "MGIapViewController.h"
#import "MGPushViewController.h"
#import "WebViewController.h"
#import "MGInviteViewController.h"
#import <MugenContainer/MugenContainer.h>
#import <SVProgressHUD/SVProgressHUD.h>
#import "define.h"
#import "MGUserManager.h"

@interface MGHomeViewController () <UITableViewDelegate, UITableViewDataSource,MugenContainerLoginResponseInterface,MugenContainerImLoginCallback>

@property (nonatomic, strong) UITableView *tableView;

@end

@implementation MGHomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"Home";
    self.view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.tableView];
    [self setData];
    [self loginRequest];
}

- (void)setData
{
    //配置请求环境
    MugenContainerApplicationContainer *applicationContainer = [[MugenContainerApplicationContainer alloc] initWithConfig:^(MugenContainerApplicationContainerConfiguration * _Nonnull configuration) {}];
    [MugenContainerApplicationInitializer.companion initializeType:MugenContainerEnvironmentType.beta container:applicationContainer network:MugenContainerNetWork.mugen];
}

- (void)loginRequest
{
    dispatch_async(dispatch_get_main_queue(), ^{
        MugenContainerLoginRepository *loginRepository = [[MugenContainerLoginRepository alloc] init];
        [loginRepository loginLoginInterface:self completionHandler:^(NSError * _Nullable error) {
            
        }];
    });
}

#pragma mark - MugenContainerLoginResponseInterface
- (void)loginResponseUserInfo:(MugenContainerUserInfo * _Nullable)userInfo
{
    [MGUserManager sharedInstance].userInfo = userInfo;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"loginSuccess" object:self];
    NSLog(@"login success: %@,%@,%@",userInfo.customerInfo.obj.id, userInfo.inviteInfo.obj.inviteCode, userInfo.inviteInfo.obj.inviterId);
    //mugen登录成功后，登录IM，接收推送
    if(userInfo.customerInfo.obj.id.length > 0) {
        dispatch_async(dispatch_get_main_queue(), ^{
            //开发环境ImId：15565 ，生产环境：15576
            [MugenContainerImSetting.companion.INSTANCE doInitPushImId:@"15565" imAppId:@"20001744" language:nil callback:self completionHandler:^(NSError * _Nullable error) {
                
            }];
        });
    }
}

#pragma mark - MugenContainerImLoginCallback
- (void)onLoginSuccess
{
    [SVProgressHUD showSuccessWithStatus:@"IM Login Success"];
}

- (void)onLoginErrorError:(int32_t)error msg:(NSString * _Nullable)msg
{
    NSLog(@"IM Login Fail = %li,%@",(long)error, msg);
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [tableView dequeueReusableCellWithIdentifier:@"UITableViewCell"];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *homeCell = cell;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if(indexPath.row == MGHomeCellTypeLogin) {
        homeCell.textLabel.text = @"Login/UserInfo";
    } else if(indexPath.row == MGHomeCellTypeIAP) {
        homeCell.textLabel.text = @"IAP";
    } else if(indexPath.row == MGHomeCellTypeWebView) {
        homeCell.textLabel.text = @"WebViewTest";
    } else if(indexPath.row == MGHomeCellTypeInvite) {
        homeCell.textLabel.text = @"Invite Share";
    } else {
        homeCell.textLabel.text = @"Push";
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == MGHomeCellTypeLogin) {
        MGMainViewController *mvc = [[MGMainViewController alloc] init];
        [self.navigationController pushViewController:mvc animated:YES];
    } else if (indexPath.row == MGHomeCellTypeIAP) {
        MGIapViewController *ivc = [[MGIapViewController alloc] init];
        [self.navigationController pushViewController:ivc animated:YES];
    } else if (indexPath.row == MGHomeCellTypeWebView) {
        WebViewController *wvc = [[WebViewController alloc] init];
        [self.navigationController pushViewController:wvc animated:YES];
    } else if (indexPath.row == MGHomeCellTypeInvite) {
        MGInviteViewController *ivc = [[MGInviteViewController alloc] init];
        [self.navigationController pushViewController:ivc animated:YES];
    } else {
        MGPushViewController *ivc = [[MGPushViewController alloc] init];
        [self.navigationController pushViewController:ivc animated:YES];
    }
}

#pragma mark - getter

- (UITableView *)tableView
{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, UIScreen.mainScreen.bounds.size.width, UIScreen.mainScreen.bounds.size.height) style:UITableViewStyleGrouped];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        [_tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"UITableViewCell"];
        _tableView.estimatedSectionHeaderHeight = 0;
        _tableView.estimatedSectionFooterHeight = 0;
    }
    return _tableView;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
