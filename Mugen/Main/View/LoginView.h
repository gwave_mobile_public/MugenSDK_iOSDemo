//
//  LoginView.h
//  Mugen
//
//  Created by 杨鹏 on 2023/5/22.
//

#import <UIKit/UIKit.h>
#import "define.h"
NS_ASSUME_NONNULL_BEGIN

typedef void(^LoginSendBlock)(NSString *emailStr);
typedef void(^LoginVerifyBlock)(NSString *emailStr, NSString *codeStr);

@interface LoginView : UIView

@property (nonatomic, copy) LoginSendBlock loginSendBlock;
@property (nonatomic, copy) LoginVerifyBlock loginVerifyBlock;

- (void)hiddenKeyBoard;

- (void)cleanTextField;

- (void)updatePlaceholdertext:(MGMainTextFieldType)textFieldType;


@end

NS_ASSUME_NONNULL_END
