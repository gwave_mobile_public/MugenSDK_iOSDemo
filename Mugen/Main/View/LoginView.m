//
//  LoginView.m
//  Mugen
//
//  Created by 杨鹏 on 2023/5/22.
//

#import "LoginView.h"
#import <Masonry/Masonry.h>

@interface LoginView()

@property (nonatomic, strong) UITextField *emailTextField;
@property (nonatomic, strong) UIButton *sendBtn;
@property (nonatomic, strong) UITextField *codeTextField;
@property (nonatomic, strong) UIButton *verifyBtn;

@end

@implementation LoginView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)init
{
    if (self = [super init]) {
        [self setUI];
    }
    return self;
}

- (void)setUI
{
    [self addSubview:self.emailTextField];
    [self addSubview:self.sendBtn];
    [self addSubview:self.codeTextField];
    [self addSubview:self.verifyBtn];
    
    [self.emailTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(20);
        make.top.equalTo(self.mas_top).offset(10);
        make.right.equalTo(self.sendBtn.mas_left).offset(-20);
        make.height.mas_equalTo(40);
    }];
    
    [self.sendBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.mas_right).offset(-20);
        make.top.equalTo(self.emailTextField.mas_top);
        make.size.mas_equalTo(CGSizeMake(60, 40));
    }];
    
    [self.codeTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.emailTextField.mas_left);
        make.top.equalTo(self.emailTextField.mas_bottom).offset(10);
        make.right.equalTo(self.sendBtn.mas_left).offset(-20);
        make.height.mas_equalTo(40);
    }];
    
    [self.verifyBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.mas_right).offset(-20);
        make.top.equalTo(self.codeTextField.mas_top);
        make.size.mas_equalTo(CGSizeMake(60, 40));
    }];
}

- (void)cleanTextField
{
    dispatch_async(dispatch_get_main_queue(), ^{
        self.emailTextField.text = nil;
        self.codeTextField.text = nil;
    });
}

- (void)hiddenKeyBoard
{
    [self.emailTextField resignFirstResponder];
    [self.codeTextField resignFirstResponder];
}

- (void)updatePlaceholdertext:(MGMainTextFieldType)textFieldType
{
    dispatch_async(dispatch_get_main_queue(), ^{
        switch (textFieldType) {
            case MGMainTextFieldTypeDefault:
                self.emailTextField.placeholder = @"Input Email";
                self.codeTextField.placeholder = @"Input Verify Email Code";
                break;
            case MGMainTextFieldTypeOld:
                self.emailTextField.placeholder = @"Input Old Email";
                self.codeTextField.placeholder = @"Input Verify Old Email Code";
                break;
            case MGMainTextFieldTypeNew:
                self.emailTextField.placeholder = @"Input New Email";
                self.codeTextField.placeholder = @"Input Verify New Email Code";
                break;
            default:
                break;
        }
    });
}


- (void)sendClick
{
    [self hiddenKeyBoard];
    if (self.emailTextField.text.length <= 0){
        return;
    }
    if (self.loginSendBlock) {
        self.loginSendBlock(self.emailTextField.text);
    }
}

- (void)verifyClick
{
    [self hiddenKeyBoard];
    if (self.emailTextField.text.length <= 0 || self.codeTextField.text.length <= 0){
        return;
    }
    
    if (self.loginVerifyBlock){
        self.loginVerifyBlock(self.emailTextField.text, self.codeTextField.text);
    }
}

#pragma mark - get

- (UITextField *)emailTextField
{
    if (!_emailTextField) {
        _emailTextField = [[UITextField alloc] init];
        _emailTextField.placeholder = @"input email";
        _emailTextField.textColor = [UIColor blackColor];
        _emailTextField.keyboardType = UIKeyboardTypeDefault;
        _emailTextField.borderStyle = UITextBorderStyleRoundedRect;
        _emailTextField.clearButtonMode = UITextFieldViewModeAlways;
    }
    return _emailTextField;
}

- (UIButton *)sendBtn
{
    if (!_sendBtn) {
        _sendBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_sendBtn setTitle:@"SEND" forState:UIControlStateNormal];
        [_sendBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _sendBtn.backgroundColor = [UIColor purpleColor];
        _sendBtn.titleLabel.font = [UIFont systemFontOfSize:13];
        _sendBtn.layer.cornerRadius = 4.0f;
        [_sendBtn addTarget:self action:@selector(sendClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _sendBtn;
}

- (UITextField *)codeTextField
{
    if (!_codeTextField) {
        _codeTextField = [[UITextField alloc] init];
        _codeTextField.placeholder = @"input verify email code";
        _codeTextField.textColor = [UIColor blackColor];
        _codeTextField.keyboardType = UIKeyboardTypeNumberPad;
        _codeTextField.borderStyle = UITextBorderStyleRoundedRect;
        _codeTextField.clearButtonMode = UITextFieldViewModeAlways;
    }
    return _codeTextField;
}

- (UIButton *)verifyBtn
{
    if (!_verifyBtn) {
        _verifyBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_verifyBtn setTitle:@"VERIFY" forState:UIControlStateNormal];
        [_verifyBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _verifyBtn.backgroundColor = [UIColor purpleColor];
        _verifyBtn.titleLabel.font = [UIFont systemFontOfSize:13];
        _verifyBtn.layer.cornerRadius = 4.0f;
        [_verifyBtn addTarget:self action:@selector(verifyClick) forControlEvents:UIControlEventTouchUpInside];

    }
    return _verifyBtn;
}
@end
