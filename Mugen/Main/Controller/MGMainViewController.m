//
//  ViewController.m
//  Mugen
//
//  Created by 杨鹏 on 2023/5/9.
//

#import "MGMainViewController.h"
#import <Masonry/Masonry.h>
#import <MugenContainer/MugenContainer.h>
#import "MGSettingViewController.h"
#import "LoginView.h"
#import "define.h"

@interface MGMainViewController ()<MugenContainerWeb3AuthInterface,MugenContainerReCaptureResponseInterface,MugenContainerImLoginCallback>

//UI
@property (nonatomic, strong) LoginView *loginView;
@property (nonatomic, strong) UILabel *detailLabel;
@property (nonatomic, copy) UIButton *googleBtn;
@property (nonatomic, copy) UIButton *facebookBtn;
@property (nonatomic, copy) UIButton *appleBtn;
@property (nonatomic, copy) UIButton *twitchBtn;

//data
@property (nonatomic, copy) NSString *deviceInfo;//fingerprint获取到的deviceID
@property (nonatomic, copy) NSString *web3AuthClientId;
@property (nonatomic, strong) MugenContainerWeb3AuthClient *web3AuthClient;
@property (nonatomic, assign) MGMainType mainType;
@property (nonatomic, copy) NSString *tokenId;//换绑邮箱需要的tokenId
@property (nonatomic, copy) NSString *emailStr;
@property (nonatomic, assign) BOOL isWeb3AuthLogin;
@end

@implementation MGMainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"Main";
    self.view.backgroundColor = [UIColor whiteColor];
    [self setupUI];
    [self setData];
}

#pragma mark - action

- (void)sendRequest:(NSString *)emailStr
{
    //google人机验证
    dispatch_async(dispatch_get_main_queue(), ^{
        self.emailStr = emailStr;
        MugenContainerVerifyView *verifyView = [[MugenContainerVerifyView alloc] init];
        [verifyView configVersion:MugenContainerReCaptureVersion.versionV3 language:@"hk" listener:self];
    });
}

- (void)verifyRequest:(NSString *)emailStr codeStr:(NSString *)codeStr
{
    dispatch_async(dispatch_get_main_queue(), ^{
        MugenContainerLoginRepository *loginRepository = [[MugenContainerLoginRepository alloc] init];
        __weak typeof(self) weakSelf = self;
        if (self.mainType == MGMainTypeDefault || self.mainType == MGMainTypeUpdateAccount) {
            MugenContainerInt *tokenType = [[MugenContainerInt alloc] initWithInt:2];
            [loginRepository quickLoginEmail:emailStr mobile:nil password:nil smsVerifyCode:nil emailVerifyCode:codeStr tokenType:tokenType source:nil token:@"12" pTokenType:@"ios" deviceId:nil userAgent:nil smsType:nil inviteCode:nil autoRegister:self.mainType == MGMainTypeDefault?YES:NO completionHandler:^(MugenContainerUserInfo * _Nullable response, NSError * _Nullable error) {
                if (response.customerInfo.success.boolValue) {
                    [weakSelf showResponse:[NSString stringWithFormat:@"%@,%@",response.customerInfo.obj,response.inviteInfo.obj]];
                    if (weakSelf.mainType == MGMainTypeUpdateAccount) {
                        //切换IM账号
                        [weakSelf updateImUser];
                    }
                } else {
                    [weakSelf showAlertMessage:response.customerInfo.msgKey];
                }
            }];
        } else if (self.mainType == MGMainTypeBindEmail) {
            [loginRepository emailBindEmail:emailStr verifyCode:codeStr type:MugenContainerBindType.bind tokenId:nil completionHandler:^(MugenContainerWebResultCustomerVO * _Nullable response, NSError * _Nullable error) {
                if (response.success.boolValue) {
                    [weakSelf.loginView cleanTextField];
                    [weakSelf showResponse:[NSString stringWithFormat:@"%@",response.obj]];
                } else {
                    [weakSelf showAlertMessage:response.msgKey];
                }
            }];
        } else if (self.mainType == MGMainTypeChangeBindEmail) {
            if (self.tokenId.length > 0) {
                [loginRepository emailBindEmail:emailStr verifyCode:codeStr type:MugenContainerBindType.changeBind tokenId:self.tokenId completionHandler:^(MugenContainerWebResultCustomerVO * _Nullable response, NSError * _Nullable error) {
                    if (response.success.boolValue) {
                        weakSelf.tokenId = nil;
                        [weakSelf showResponse:[NSString stringWithFormat:@"%@",response.obj]];
                        [weakSelf.loginView cleanTextField];
                        [weakSelf.loginView updatePlaceholdertext:MGMainTextFieldTypeDefault];
                    } else {
                        [weakSelf showAlertMessage:response.msgKey];
                    }
                }];
                return;
            }
            //调用verify接口验证老邮箱正确性
            [loginRepository verifyVerifyCode:codeStr type:MugenContainerBindType.changeBind email:emailStr completionHandler:^(MugenContainerWebResultCustomerVerifyVO * _Nullable response, NSError * _Nullable error) {
                if (response.success.boolValue) {
                    [weakSelf showResponse:[NSString stringWithFormat:@"%@",response.obj]];
                    //获取到返回的tokenId
                    weakSelf.tokenId = response.obj.tokenId;
                    [weakSelf.loginView cleanTextField];
                    [weakSelf.loginView updatePlaceholdertext:MGMainTextFieldTypeNew];
                } else {
                    [weakSelf showAlertMessage:response.msgKey];
                }
            }];
        }
    });
}

- (void)web3AuthLogin:(UIButton *)btn
{
    NSInteger tag = btn.tag;
    MugenContainerSignInProvider *signType = MugenContainerSignInProvider.google;
    switch (tag) {
        case 100:
            signType = MugenContainerSignInProvider.google;
            break;
        case 101:
            signType = MugenContainerSignInProvider.facebook;
            break;
        case 102:
            signType = MugenContainerSignInProvider.apple;
            break;
        case 103:
            signType = MugenContainerSignInProvider.twitch;
            break;
        default:
            break;
    }
    
    [self.web3AuthClient signInSignProvider:signType email:nil walletType:MugenContainerWalletType.evm web3AuthInterface:self];
    
}

- (void)updateImUser
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [MugenContainerImSetting.companion.INSTANCE imResetLoginCallback:self completionHandler:^(NSError * _Nullable error) {
            
        }];
    });
}

- (void)settingClick
{
    MGSettingViewController *mgvc = [[MGSettingViewController alloc] init];
    __weak typeof(self) weakSelf = self;
    mgvc.settingGobackBlock = ^(MGMainType mainType) {
        weakSelf.mainType = mainType;
        [weakSelf updateUI];
    };
    [self.navigationController pushViewController:mgvc animated:YES];
}

- (void)updateUI
{
    switch (self.mainType) {
        case MGMainTypeDefault:
            self.title = @"Main";
            break;
        case MGMainTypeBindEmail:
            self.title = @"Email绑定";
            break;
        case MGMainTypeBindWeb3Auth:
            self.title = @"web3Auth绑定";
            break;
        case MGMainTypeChangeBindWeb3Auth:
            self.title = @"web3Auth换绑";
            break;
        case MGMainTypeUpdateAccount:
            self.title = @"切换账号";
            [self.loginView updatePlaceholdertext:MGMainTextFieldTypeDefault];
            break;
        case MGMainTypeChangeBindEmail: {
            //更新换绑邮箱的UI
            self.title = @"Email换绑";
            self.tokenId = nil;
            [self.loginView updatePlaceholdertext:MGMainTextFieldTypeOld];
        }
            break;
        case MGMainTypeLogout:
            self.title = @"Main";
            [self.loginView updatePlaceholdertext:MGMainTextFieldTypeDefault];
            [self showResponse:@"Logout Success"];
            [self.loginView cleanTextField];
            if (self.isWeb3AuthLogin) {
                self.isWeb3AuthLogin = NO;
                [self.web3AuthClient signOut];
            }
            self.mainType = MGMainTypeDefault;
            break;
        default:
            break;
    }
}

#pragma mark - MugenContainerImLoginCallback
- (void)onLoginSuccess
{
    NSLog(@"IM Login Success");
}

- (void)onLoginErrorError:(int32_t)error msg:(NSString * _Nullable)msg
{
    NSLog(@"IM Login Fail = %li,%@",(long)error, msg);
}

#pragma mark -MugenContainerWeb3AuthInterface
- (void)web3AuthResponseWeb3AuthResponse:(MugenContainerAuthResponse *)web3AuthResponse
{
    if(web3AuthResponse.idToken.length> 0 && web3AuthResponse.publicKey.length > 0) {
        dispatch_async(dispatch_get_main_queue(), ^{
            MugenContainerLoginRepository *loginRepository = [[MugenContainerLoginRepository alloc] init];
            __weak typeof(self) weakSelf = self;
            if (self.mainType == MGMainTypeDefault || self.mainType == MGMainTypeUpdateAccount) {
                [loginRepository oauthLoginIdToken:web3AuthResponse.idToken publicKey:web3AuthResponse.publicKey autoRegister:self.mainType == MGMainTypeDefault?YES:NO thirdAuthSource:MugenContainerThirdAuthType.externalSocial walletType:MugenContainerWalletType.evm completionHandler:^(MugenContainerUserInfo * _Nullable response, NSError * _Nullable error) {
                    if (response.customerInfo.success.boolValue) {
                        weakSelf.isWeb3AuthLogin = YES;
                        [weakSelf showResponse:[NSString stringWithFormat:@"%@,%@",response.customerInfo.obj, response.inviteInfo.obj]];
                        if (weakSelf.mainType == MGMainTypeUpdateAccount) {
                            //切换IM账号
                            [weakSelf updateImUser];
                        }
                    } else {
                        [weakSelf showAlertMessage:response.customerInfo.msgKey];
                    }
                }];
            } else if (self.mainType == MGMainTypeBindWeb3Auth || self.mainType == MGMainTypeChangeBindWeb3Auth) {
                [loginRepository oauthBindIdToken:web3AuthResponse.idToken thirdAuthSource:MugenContainerThirdAuthType.externalSocial platform:@"web3auth" walletType:MugenContainerWalletType.evm completionHandler:^(MugenContainerWebResult * _Nullable response, NSError * _Nullable error) {
                    if (response.success.boolValue) {
                        weakSelf.isWeb3AuthLogin = YES;
                        [weakSelf showResponse:[NSString stringWithFormat:@"%@",response.msgKey]];
                    } else {
                        [weakSelf showAlertMessage:response.msgKey];
                    }
                }];
            }
        });
    } else {
        [self showAlertMessage:@"idToken or publicKey is null，login fail"];
    }
}

#pragma mark - MugenContainerReCaptureResponseInterface
- (void)responseResponse:(NSString *)response
{
    dispatch_async(dispatch_get_main_queue(), ^{
        MugenContainerLoginRepository *loginRepository = [[MugenContainerLoginRepository alloc] init];
        __weak typeof(self) weakSelf = self;
        [loginRepository getEmailCodeEmail:self.emailStr completionHandler:^(MugenContainerWebResponse * _Nullable response, NSError * _Nullable error) {
            if (response.success.boolValue) {
                [weakSelf showResponse:response.msgKey];
            } else {
                [weakSelf showAlertMessage:response.msgKey];
            }
        }];
    });
}

- (void)showAlertMessage:(NSString *)showMessage
{
    dispatch_async(dispatch_get_main_queue(), ^{
        if (showMessage.length > 0) {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Message" message:showMessage preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction * okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:okAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
    });
}

- (void)showResponse:(NSString *)response
{
    dispatch_async(dispatch_get_main_queue(), ^{
        if (response.length > 0) {
            self.detailLabel.text = response;
        }
    });
}

#pragma mark - init
- (void)setData
{
    self.isWeb3AuthLogin = NO;
    self.mainType = MGMainTypeDefault;
    //初始化web3Auth
    self.web3AuthClientId = @"BCP0RdVtKYtjMkKht8KfOvuRVoi-Xs4Di-iKwO04gA2AoNB5dt0eCtduhkF-wQpZXb4_jTgCAz6FwgHRcaL1jrw";
    [self.web3AuthClient doInitWeb3AuthClientId:self.web3AuthClientId web3Network:MugenContainerWeb3AuthNetWork.testnet url:@"https://kikitrade.com" uri:@"com.mugen.odyssey"];
}

- (void)setupUI
{
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithTitle:@"Setting" style:UIBarButtonItemStyleDone target:self action:@selector(settingClick)];
    self.navigationItem.rightBarButtonItem = rightItem;
    [self.view addSubview:self.googleBtn];
    [self.view addSubview:self.facebookBtn];
    [self.view addSubview:self.appleBtn];
    [self.view addSubview:self.twitchBtn];
    [self.view addSubview:self.loginView];
    [self.view addSubview:self.detailLabel];
    
    [self.googleBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view.mas_top).offset(110);
        make.left.equalTo(self.view.mas_left).offset(20);
        make.size.mas_equalTo(CGSizeMake((UIScreen.mainScreen.bounds.size.width - 100)/4, 40));
    }];
    
    [self.facebookBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.googleBtn.mas_top).offset(0);
        make.left.equalTo(self.googleBtn.mas_right).offset(20);
        make.size.mas_equalTo(CGSizeMake((UIScreen.mainScreen.bounds.size.width - 100)/4, 40));
    }];
    
    [self.appleBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.googleBtn.mas_top).offset(0);
        make.left.equalTo(self.facebookBtn.mas_right).offset(20);
        make.size.mas_equalTo(CGSizeMake((UIScreen.mainScreen.bounds.size.width - 100)/4, 40));
    }];
    
    [self.twitchBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.googleBtn.mas_top).offset(0);
        make.left.equalTo(self.appleBtn.mas_right).offset(20);
        make.size.mas_equalTo(CGSizeMake((UIScreen.mainScreen.bounds.size.width - 100)/4, 40));
    }];
    
    [self.loginView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.googleBtn.mas_bottom).offset(10);
        make.left.equalTo(self.view.mas_left).offset(0);
        make.right.equalTo(self.view.mas_right).offset(0);
        make.height.mas_equalTo(110);
    }];
    
    [self.detailLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view.mas_left).offset(10);
        make.right.mas_equalTo(self.view.mas_right).offset(-20);
        make.top.equalTo(self.loginView.mas_bottom).offset(10);
    }];
}

#pragma mark - getter

- (LoginView *)loginView
{
    if (!_loginView) {
        _loginView = [[LoginView alloc] init];
        __weak typeof(self) weakSelf = self;
        _loginView.loginSendBlock = ^(NSString * _Nonnull emailStr) {
            [weakSelf sendRequest:emailStr];
        };
        _loginView.loginVerifyBlock = ^(NSString * _Nonnull emailStr, NSString * _Nonnull codeStr) {
            [weakSelf verifyRequest:emailStr codeStr:codeStr];
        };
    }
    return _loginView;
}

- (UILabel *)detailLabel
{
    if (!_detailLabel) {
        _detailLabel = [[UILabel alloc] init];
        _detailLabel.text = @"show result";
        _detailLabel.textAlignment = NSTextAlignmentLeft;
        _detailLabel.textColor = [UIColor grayColor];
        _detailLabel.font = [UIFont systemFontOfSize:15];
        _detailLabel.numberOfLines = 0;
    }
    return _detailLabel;
}

- (UIButton *)googleBtn
{
    if (!_googleBtn) {
        _googleBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_googleBtn setTitle:@"Google" forState:UIControlStateNormal];
        [_googleBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _googleBtn.backgroundColor = [UIColor purpleColor];
        _googleBtn.titleLabel.font = [UIFont systemFontOfSize:13];
        _googleBtn.layer.cornerRadius = 4.0f;
        _googleBtn.tag = 100;
        [_googleBtn addTarget:self action:@selector(web3AuthLogin:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _googleBtn;
}

- (UIButton *)facebookBtn
{
    if (!_facebookBtn) {
        _facebookBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_facebookBtn setTitle:@"FaceBook" forState:UIControlStateNormal];
        [_facebookBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _facebookBtn.backgroundColor = [UIColor purpleColor];
        _facebookBtn.titleLabel.font = [UIFont systemFontOfSize:13];
        _facebookBtn.layer.cornerRadius = 4.0f;
        _facebookBtn.tag = 101;
        [_facebookBtn addTarget:self action:@selector(web3AuthLogin:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _facebookBtn;
}

- (UIButton *)appleBtn
{
    if (!_appleBtn) {
        _appleBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_appleBtn setTitle:@"Apple" forState:UIControlStateNormal];
        [_appleBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _appleBtn.backgroundColor = [UIColor purpleColor];
        _appleBtn.titleLabel.font = [UIFont systemFontOfSize:13];
        _appleBtn.layer.cornerRadius = 4.0f;
        _appleBtn.tag = 102;
        [_appleBtn addTarget:self action:@selector(web3AuthLogin:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _appleBtn;
}

- (UIButton *)twitchBtn
{
    if (!_twitchBtn) {
        _twitchBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_twitchBtn setTitle:@"Twitch" forState:UIControlStateNormal];
        [_twitchBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _twitchBtn.backgroundColor = [UIColor purpleColor];
        _twitchBtn.titleLabel.font = [UIFont systemFontOfSize:13];
        _twitchBtn.layer.cornerRadius = 4.0f;
        _twitchBtn.tag = 103;
        [_twitchBtn addTarget:self action:@selector(web3AuthLogin:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _twitchBtn;
}

- (MugenContainerWeb3AuthClient *)web3AuthClient
{
    if(!_web3AuthClient) {
        _web3AuthClient = [[MugenContainerWeb3AuthClient alloc] init];
    }
    return _web3AuthClient;
}
@end
