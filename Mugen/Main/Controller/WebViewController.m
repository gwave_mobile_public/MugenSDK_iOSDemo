//
//  WebViewController.m
//  Mugen
//
//  Created by 杨鹏 on 2023/5/26.
//

#import "WebViewController.h"
#import <WebKit/WebKit.h>
#import <Masonry/Masonry.h>
#import <SVProgressHUD/SVProgressHUD.h>

@interface WebViewController ()<WKNavigationDelegate,WKScriptMessageHandler,WKUIDelegate>

@property (nonatomic, strong) WKWebView *webView;

@end

@implementation WebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
//    [self.view addSubview:self.webView];

//    //https://m.dipbit.xyz/captcha/google-v3.html
//    //https://m.dipbit.xyz/captcha/index.html?language=hk
    NSString *urlStr = @"https://m.dipbit.xyz/captcha/google-v3.html";
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:urlStr]];
    [self.webView loadRequest:urlRequest]; // 加载页面
    
//
//    NSString *PageUrlString = [NSString stringWithFormat:@"file://%@",[[NSBundle mainBundle] pathForResource:@"test" ofType:@"html"]];
//    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:PageUrlString]]];
    
//    NSString *path = [[NSBundle mainBundle] pathForResource:@"test" ofType:@"html"];
//    NSString *htmlString = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
//    NSString *basePath = [[NSBundle mainBundle] bundlePath];
//    NSURL *baseURL = [NSURL fileURLWithPath:basePath];
//    [self.webView loadHTMLString:htmlString baseURL:baseURL];

//    NSString *path = [[NSBundle mainBundle] pathForResource:@"test" ofType:@"html"];
//    NSURL *htmlURL = [NSURL fileURLWithPath:path];
//    [self.webView loadFileURL:htmlURL allowingReadAccessToURL:htmlURL];

}

#pragma  mark -WKNavigationDelegate
// 页面开始加载时调用
- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation
{
    NSLog(@"start Loading...");
}
// 当内容开始返回时调用
- (void)webView:(WKWebView *)webView didCommitNavigation:(WKNavigation *)navigation
{
    NSLog(@"data return...");
}
// 页面加载完成之后调用
- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation
{
    NSLog(@"loading finish...");
}
// 页面加载失败时调用
- (void)webView:(WKWebView *)webView didFailProvisionalNavigation:(WKNavigation *)navigation
{
    NSLog(@"fail...");
}


- (WKWebView *)webView {
    if (_webView == nil) {
        WKUserContentController *userController = [[WKUserContentController alloc] init];
        [userController addScriptMessageHandler:self name:@"JSBridge"];
        WKWebViewConfiguration *config = [[WKWebViewConfiguration alloc] init];
        config.userContentController = userController;
//        [config.userContentController addScriptMessageHandler:self name:@"JSBridge"];
        _webView = [[WKWebView alloc] initWithFrame:CGRectMake(0, 0, UIScreen.mainScreen.bounds.size.width, UIScreen.mainScreen.bounds.size.height) configuration:config];
        _webView.navigationDelegate = self;
        _webView.UIDelegate = self;
        _webView.backgroundColor = [UIColor whiteColor];
    }
    return _webView;
}

#pragma WKScriptMessageHandler
- (void)userContentController:(nonnull WKUserContentController *)userContentController didReceiveScriptMessage:(nonnull WKScriptMessage *)message {
    NSLog(@"---------%@,%@",message.name,message.body);
    [SVProgressHUD showSuccessWithStatus:message.name];
}

@end
